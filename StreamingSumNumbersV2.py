from __future__ import print_function

import sys
from operator import add

from pyspark import SparkContext
from pyspark.streaming import StreamingContext

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: StreamingSumNumbersV2.py <hostname> <port>", file=sys.stderr)
        exit(-1)
    sc = SparkContext("local[2]", appName="StreamingSumNumbers")
    ssc = StreamingContext(sc, 1)

    accum = sc.accumulator(0)

    lines = ssc.socketTextStream(sys.argv[1], int(sys.argv[2]))
    counts = lines.flatMap(lambda line: line.split(" "))
    pairs = counts.map(lambda num: int(num))
    sumnum = pairs.reduce(add)


    def get_output(rdd):
        datas = rdd.collect()
        total = 0
        global accum
        for x in datas:
            accum.add(int(x))

        print("Total sum: ", accum.value)


    sumnum.pprint()
    sumnum.foreachRDD(get_output)

    ssc.start()
    ssc.awaitTermination()