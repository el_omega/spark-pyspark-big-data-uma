from __future__ import print_function

from pyspark import SparkContext
from pyspark.mllib.linalg import  DenseVector
from pyspark.mllib.clustering import KMeans
import sys


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Syntax Error: directory with the model missing", file=sys.stderr)
        exit(-1)

    sc = SparkContext(appName="KMeansClustering")
    path = sys.argv[1]
    data = sc.textFile(path).cache()

    parsedData = data.map(lambda line: DenseVector([float(x) for x in line.split(" ")]))
    parsedData.cache()
    print(parsedData)
    numClusters = 2
    numIterations = 20
    model = KMeans.train(parsedData, numClusters, numIterations)
    model.save(sc, "KmeansModelPath")
