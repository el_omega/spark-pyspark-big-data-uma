from __future__ import print_function
from pyspark import SparkContext
from pyspark.mllib.linalg import  DenseVector
from pyspark.mllib.clustering import  KMeansModel
import sys

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Syntax Error: directory with the model missing", file=sys.stderr)
        exit(-1)

    sc = SparkContext(appName="StreamingWordCount")
    path = sys.argv[1]

    model = KMeansModel.load(sc,path)

    v = DenseVector([1.1, 3.2])
    print("Vector 1: ", v)
    print("Data 1: ", model.predict(v))

    v = DenseVector([5.1, 1.4])
    print("Vector 1: ", v)
    print("Data 1: ", model.predict(v))

    v = DenseVector([5.2, 2])
    print("Vector 1: ", v)
    print("Data 1: ", model.predict(v))
