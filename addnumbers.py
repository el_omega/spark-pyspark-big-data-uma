from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: wordcount <file>", file=sys.stderr)
        exit(-1)

    spark = SparkSession \
        .builder \
        .appName("AddNumbers") \
        .getOrCreate()

    lines = spark.read.text(sys.argv[1]).rdd.map(lambda r: r[0])
    counts = lines.map(lambda s: ("The sum is: ", s))\
        .reduceByKey(add)


    output = counts.first()
    for (word, count) in output:
        print("%s %i" % (word, count))

    spark.stop()