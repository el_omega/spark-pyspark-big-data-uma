from __future__ import print_function

from pyspark import SparkContext
from pyspark.mllib.classification import SVMWithSGD
from pyspark.mllib.util import MLUtils
from pyspark.mllib.evaluation import BinaryClassificationMetrics

import sys


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Syntax Error: there must be one argument (a libsvm data file)", file=sys.stderr)
        exit(-1)

    sc = SparkContext(appName="SVM Classifier Example")
    path = sys.argv[1]
    data = MLUtils.loadLibSVMFile(sc, path)
    splits = data.randomSplit([0.6, 0.4], 11)
    training = splits[0].cache()
    test = splits[1].cache()

    print("Test points size: ", test.count())

    numIterations = 100
    model = SVMWithSGD.train(training, numIterations)


    # Evaluating the model on training data
    labelsAndPreds = test.map(lambda p: (p.label, float(model.predict(p.features))) )

    trainErr = labelsAndPreds.filter(lambda p: (p[0] != p[1])).count() / float(test.count())

    print("Datos test")
    print(labelsAndPreds)
    print("Training Error = " + str(trainErr))
    metrics = BinaryClassificationMetrics(labelsAndPreds)


    auROC = metrics.areaUnderROC
    print("Area under ROC = " , auROC)
    # Save and load model
    model.save(sc, "SVMWithSGDModel")
