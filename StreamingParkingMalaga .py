from __future__ import print_function

import sys

from operator import add
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

if __name__ == "__main__":
    if len(sys.argv) == 0:
        print("Usage: StreamingParkingMalaga <folder> ", file=sys.stderr)
        exit(-1)
    sc = SparkContext(appName="StreamingParkingMalaga")
    ssc = StreamingContext(sc, 60)

    lines = ssc.textFileStream(sys.argv[1])

    data = lines.map(lambda line: [line.split(",")[1],line.split(",")[8],line.split(",")[10],
                     line.split(",")[11]])

    def get_output(rdd):
       datas = rdd.collect()
       for x in datas:
           if(x[0] != "\"nombre\""):
               print("Name: ", x[0], "Capacity: ", x[1],"Time: ", x[2], "Free lots: ", x[3] )
               print()
               print("-----------------------------------------------------------------------------------")
               print()


    data.foreachRDD(get_output)



    ssc.start()
    ssc.awaitTermination()
