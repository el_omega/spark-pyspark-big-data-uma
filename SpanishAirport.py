from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: SpanishAirport <file>", file=sys.stderr)
        exit(-1)

    spark = SparkSession \
        .builder \
        .appName("Spanish Airport") \
        .getOrCreate()

    lines = spark.read.text(sys.argv[1]).rdd.map(lambda r: r[0])
    #Split all the lines of the document by:
    split = lines.map(lambda line: line.split(","))

    total = split.filter(lambda s: True if s[8] == "\"ES\"" else False) \
        .map(lambda field: (field[2],1)).reduceByKey(add).sortByKey().collect()


    for (word, count) in total:
        print("%s: %i" % (word, count))




    spark.stop()