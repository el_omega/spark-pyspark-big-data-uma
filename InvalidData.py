from __future__ import print_function

import sys


from pyspark.sql import SparkSession

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: InvalidData.py <file>", file=sys.stderr)
        exit(-1)

    spark = SparkSession \
        .builder \
        .appName("InvalidData") \
        .getOrCreate()



    def is_number(s):
            try:
                float(s)
                return True
            except ValueError:
                pass

            try:
                import unicodedata
                unicodedata.numeric(s)
                return True
            except (TypeError, ValueError):
                pass
            return False

    lines = spark.read.text(sys.argv[1]).rdd.map(lambda r: r[0])

    valid = lines.filter(lambda s: True if is_number(s) else False)

    output = valid.collect()

    for key in output:
        print (key)

    spark.stop()