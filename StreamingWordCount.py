from __future__ import print_function

import sys

from pyspark import SparkContext
from pyspark.streaming import StreamingContext

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: StreamingWordCount.py <hostname> <port>", file=sys.stderr)
        exit(-1)
    sc = SparkContext("local[2]",appName="StreamingWordCount")
    ssc = StreamingContext(sc, 1)

    lines = ssc.socketTextStream(sys.argv[1], int(sys.argv[2]))
    counts = lines.flatMap(lambda line: line.split(" "))
    pairs = counts.map(lambda word: (word, 1))
    wordcounts = pairs.reduceByKey(lambda a, b: a+b)
    wordcounts.pprint()

    ssc.start()
    ssc.awaitTermination()