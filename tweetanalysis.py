from __future__ import print_function

import sys
from operator import add

import numpy as np
from nltk.corpus import stopwords

from pyspark.sql import SparkSession

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: tweetanalysis.py <file>", file=sys.stderr)
        exit(-1)

    spark = SparkSession \
        .builder \
        .appName("Tweet Analysis") \
        .getOrCreate()

    lines = spark.read.text(sys.argv[1]).rdd.map(lambda r: r[0])
    #Split all the lines of the document by:
    split = lines.map(lambda line: line.split("\t"))

    #LONGEST TWEET

    #Get the column where is the tweet
    tweet = split.map(lambda fields: fields[2])
    alltweets = tweet.collect()

    #Get the length of the tweets and sort them
    maptweet = tweet.map(lambda s: (s,len(s)))
    sorttweet = maptweet.map(lambda s: (s[1],s[0])).sortByKey(False).first()

    #Get the information of user and date of tweets:
    users =  split.map(lambda s: s[1]).collect()
    dates = split.map(lambda s: s[3]).collect()

    #MOST ACTIVE USER

    allusers = split.map(lambda s: s[1])
    mapusers = allusers.map(lambda s: (s,1)).reduceByKey(add)
    sortusers = mapusers.map(lambda s: (s[1],s[0])).sortByKey(False).first()


    #TRENDIG TOPIC
    stop = set(stopwords.words('english'))


    splittweet = tweet.flatMap(lambda s: np.array(s.split((" "))).tolist())
    filtertweet = splittweet.filter(lambda s: True if s not in stop else False)
    splitwords = filtertweet.map(lambda s: (s,1)).reduceByKey(add)
    words = splitwords.map(lambda s: (s[1],s[0])).sortByKey(False).take(11)

    for (word, count) in words:
        print("%s: %s" % (word, count))



    print ("The users who has written the amount of tweets is: %s   Total tweets: %i" %(sortusers[1],sortusers[0]) )

    for j in range(len(users)-1):
        if(sorttweet[1] == alltweets[j]):
            print("User: %s" %(users[j]))
            print("Tweet: %s" %(sorttweet[1]))
            print("Num of characters: %s" %(sorttweet[0]))
            print("Date: %s" %(dates[j]))
            break



    spark.stop()