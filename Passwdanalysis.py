from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: Passwdanalysis <file>", file=sys.stderr)
        exit(-1)

    spark = SparkSession \
        .builder \
        .appName("Passwd Analysis") \
        .getOrCreate()

    lines = spark.read.text(sys.argv[1]).rdd.map(lambda r: r[0])
    #Split all the lines of the document by:
    split = lines.map(lambda line: line.split(":"))

    #Map the first colum to get the users, then count
    numusers = split.map(lambda fields: fields[0]).count()

    print("The numbers of user accounts is : %i" % (numusers))

    #Get the elements of each column (users,UID, GUID)
    users = split.map(lambda fields: fields[0])
    UID = split.map(lambda fields: fields[2])
    GUID = split.map(lambda fields: fields[3])

    #Sort the users by his first word, then take the five first and show the information
    sortuser = users.sortBy(lambda s: s[0]).take(6)
    userList= users.collect()
    UIDList= UID.collect()
    GUIDList = GUID.collect()


    for j in range(len(sortuser)-1):
        for i in range(len(userList)-1):
            if(sortuser[j] == userList[i]):
                print ( "User name: %s  UID: %s  GUI: %s" %(userList[i], UIDList[i], GUIDList[i]))

    #Map the last colum, where is the information /bin/bash, then filter and count how many times appear

    binbas= split.map(lambda field: field[6]).filter(lambda field: True if "/bin/bash" in field else False).count()

    print ("The numbers of user having bash as command when login is : %s" %(binbas))



    spark.stop()