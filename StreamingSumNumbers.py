from __future__ import print_function

import sys
from operator import add
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: StreamingSumNumbers.py <hostname> <port>", file=sys.stderr)
        exit(-1)
    sc = SparkContext("local[2]",appName="StreamingSumNumbers")
    ssc = StreamingContext(sc, 1)

    lines = ssc.socketTextStream(sys.argv[1], int(sys.argv[2]))
    counts = lines.flatMap(lambda line: line.split(" "))
    pairs = counts.map(lambda num: int(num))
    sumnum = pairs.reduce(add)
    sumnum.pprint()

    ssc.start()
    ssc.awaitTermination()