from __future__ import print_function

from pyspark import SparkContext
from pyspark.mllib.classification import SVMModel
from pyspark.mllib.linalg import DenseVector


import sys


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Syntax Error: there must be one argument (a libsvm data file)", file=sys.stderr)
        exit(-1)

    sc = SparkContext(appName="SVM Classifier Example")
    path = sys.argv[1]

    model = SVMModel.load(sc,path)
    print(model)

    v = DenseVector([1.1, 3.2])
    print("Vector 1: ", v)
    print("Data 1: ", model.predict(v))

    v = DenseVector([5.1, 1.4])
    print("Vector 1: ", v)
    print("Data 1: ", model.predict(v))

    v = DenseVector([5.2, 2])
    print("Vector 1: ", v)
    print("Data 1: ", model.predict(v))