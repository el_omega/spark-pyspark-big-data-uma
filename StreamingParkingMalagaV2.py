from __future__ import print_function

import sys

from operator import add
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

if __name__ == "__main__":
    if len(sys.argv) == 0:
        print("Usage: StreamingParkingMalaga <folder> ", file=sys.stderr)
        exit(-1)
    sc = SparkContext(appName="StreamingParkingMalaga")
    ssc = StreamingContext(sc, 60)

    lines = ssc.textFileStream(sys.argv[1])

    data = lines.map(lambda line: [line.split(",")[1],line.split(",")[10],line.split(",")[8],
                     line.split(",")[11]])

    def get_output(rdd):
       if(len(rdd.collect()) != 0 ):

           datas = rdd.collect()
           totalfree =0
           totalcapacity =0
           min_free = -2
           max_free = -2
           nombre_min = ""
           nombre_max = ""


           for x in datas:
               if(x[0] != "\"nombre\""):
                   if int(x[2].replace("\"", "")) == -1 or int(x[3].replace("\"", "")) == -1:
                       totalfree += 0
                       totalcapacity +=0
                   else:
                       hora = x[1]
                       if(min_free == -2):
                           min_free = int(x[3].replace("\"", ""))
                           max_free = int(x[3].replace("\"", ""))
                       else:
                           if(min_free > int(x[3].replace("\"", ""))):
                               min_free = int(x[3].replace("\"", ""))
                               nombre_min = x[0]
                           else:
                               if(max_free < int(x[3].replace("\"", ""))):
                                   max_free = int(x[3].replace("\"", ""))
                                   nombre_max = x[0]
                       totalcapacity += int(x[2].replace("\"", ""))
                       totalfree += int(x[3].replace("\"", ""))

           occupancy = (totalcapacity - totalfree)
           average_occupancy = (occupancy/totalcapacity)
           print("Last update: ", hora)
           print("Total numbers parkings lots: ", totalcapacity)
           print("Total numbers of occupied parkings lots: ", occupancy)
           print("Name min free: ", nombre_min, " Free lots: ", min_free)
           print("Name max free: ", nombre_max, " Free lots: ", max_free)
           print("Average number of occupancy of all parkings: ", average_occupancy)
           print()
           print("-----------------------------------------------------------------------------------")
           print()


    data.foreachRDD(get_output)



    ssc.start()
    ssc.awaitTermination()