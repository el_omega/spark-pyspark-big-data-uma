from __future__ import print_function

import sys
from operator import add

from pyspark import SparkContext
from pyspark.streaming import StreamingContext

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: StreamingWordCountWindow.py <hostname> <port>", file=sys.stderr)
        exit(-1)


    def functionToCreateContext():
        sc = SparkContext("local[2]", appName="StreamingWordCountWindow")
        ssc = StreamingContext(sc, 1)
        lines = ssc.socketTextStream(sys.argv[1], int(sys.argv[2]))
        counts = lines.flatMap(lambda line: line.split(" ")) \
            .map(lambda word: (word, 1)) \
            .reduceByKeyAndWindow(add, 5, 2)
        counts.pprint()
        ssc.checkpoint(checkpointDirectory)  # set checkpoint directory
        return ssc


    checkpointDirectory = "CheckPoint"
    context = StreamingContext.getOrCreate(checkpointDirectory, functionToCreateContext)
    context.start()
    context.awaitTermination()